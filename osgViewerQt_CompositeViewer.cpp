// Copyright (C) 2020 by Nelsson Huotari

/* This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

#include "osgViewerQt_CompositeViewer.hpp"

#include "osgQOpenGLWidget.hpp"

#include <osgDB/ReadFile>

#include <osg/Material>

#include <osgViewer/View>
#include <osgViewer/CompositeViewer>

#include <osgGA/TrackballManipulator>

#include <QApplication>
#include <QLayout>
#include <QDockWidget>
#include <QMainWindow>
#include <QPointer>
#include <QSurfaceFormat>

#include <iostream>

/* Qt5 use similar to OpenMW-CS */
RenderWidget::RenderWidget(QWidget *parent, Qt::WindowFlags f)
    : QWidget(parent, f)
    , mRootNode(nullptr)
{

    osg::DisplaySettings* ds = osg::DisplaySettings::instance().get();
    QSurfaceFormat format = QSurfaceFormat::defaultFormat();
    format.setVersion(2, 1);
    format.setRenderableType(QSurfaceFormat::OpenGL);
    //format.setOption(QSurfaceFormat::DebugContext);
    format.setDepthBufferSize(24);
    //format.setAlphaBufferSize(8);
    format.setSamples(ds->getMultiSamples());
    format.setStencilBufferSize(ds->getMinimumNumStencilBits());
    format.setSwapBehavior(QSurfaceFormat::DoubleBuffer);
    QSurfaceFormat::setDefaultFormat(format);

    mView = new osgViewer::View;

    mWidget = new osgQOpenGLWidget(this);

    mRenderer = mWidget->getCompositeViewer();
    osg::ref_ptr<osgViewer::GraphicsWindowEmbedded> window = new osgViewer::GraphicsWindowEmbedded(0, 0, width(), height());
    mWidget->setGraphicsWindowEmbedded(window);

    int frameRateLimit = 10;
    mRenderer->setRunMaxFrameRate(frameRateLimit);

#if OSG_VERSION_GREATER_OR_EQUAL(3,5,5)
    mRenderer->setUseConfigureAffinity(false);
#endif

    QLayout* layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    layout->addWidget(mWidget);

    setLayout(layout);

    mView->getCamera()->setGraphicsContext(window);

    mView->getCamera()->setClearColor( osg::Vec4(0.2, 0.2, 0.4, 1.0) );
    mView->getCamera()->setViewport( new osg::Viewport(0, 0, width(), height()) );

    mView->getCamera()->getOrCreateStateSet()->setMode(GL_NORMALIZE, osg::StateAttribute::ON);
    mView->getCamera()->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::ON);
    mView->getCamera()->getOrCreateStateSet()->setMode(GL_CULL_FACE, osg::StateAttribute::ON);
    osg::ref_ptr<osg::Material> defaultMat (new osg::Material);
    defaultMat->setColorMode(osg::Material::OFF);
    defaultMat->setAmbient(osg::Material::FRONT_AND_BACK, osg::Vec4f(0.9,0.5,1,1));
    defaultMat->setDiffuse(osg::Material::FRONT_AND_BACK, osg::Vec4f(1,0.9,0.8,1));
    defaultMat->setSpecular(osg::Material::FRONT_AND_BACK, osg::Vec4f(0.f, 0.f, 0.f, 0.f));
    mView->getCamera()->getOrCreateStateSet()->setAttribute(defaultMat);

    osg::ref_ptr<osgGA::TrackballManipulator> trackballManipulator = new osgGA::TrackballManipulator();
    mView->setCameraManipulator(trackballManipulator.get());

    mView->setSceneData(mRootNode);

    mRenderer->addView(mView);
    mRenderer->setDone(false);
}

RenderWidget::~RenderWidget()
{
    try
    {
        mRenderer->removeView(mView);
    }
    catch(const std::exception& e)
    {
    }
    delete mWidget;
}

std::string getFileExtension(const std::string& file)
{
        size_t extPos = file.find_last_of('.');
        if (extPos != std::string::npos && extPos+1 < file.size())
            return file.substr(extPos+1);
        return std::string();
}

/* OpenMW-CS -related code end */

QPointer<osgQOpenGLWidget> RenderWidget::getOsgQOpenGLWidget()
{
    return mWidget;
}

osgViewer::View* RenderWidget::getView(int viewNumber)
{
    return mWidget->getOsgView(viewNumber);
}

/* osgQt-origin code begin */
int main( int argc, char** argv )
{
    QApplication app(argc, argv);

    QMainWindow *main = new QMainWindow();
    QDockWidget *dock = new QDockWidget("Dock", main);
    RenderWidget *widget = new RenderWidget(dock);

    // Resize with three different methods (coding exercise)
    int mainWindowWidth = 800;
    int mainWindowHeight = 600;
    QSize mainWindowSize( mainWindowWidth, mainWindowHeight );
    main->resize(mainWindowSize);

    dock->resize(750, 550);

    int renderWindowWidth = 700;
    int renderWindowHeight = 500;
    widget->resize(renderWindowWidth, renderWindowHeight);

    //std::string filename = "scaledsign.dae";
    //std::string filename = "sign_small.fbx";
    std::string filename = "sign_small.dae";
    //std::string filename = "cow.osgt";
    std::string ext = getFileExtension(filename);
    osgDB::ReaderWriter* reader = osgDB::Registry::instance()->getReaderWriterForExtension(ext);
    if (!reader)
    {
        std::stringstream errormsg;
        errormsg << "Error with" << filename << ": no readerwriter for '" << ext << "' found" << std::endl;
        throw std::runtime_error(errormsg.str());
    }

    osg::ref_ptr<osgDB::Options> options (new osgDB::Options);
        if (ext == "dae") options->setOptionString("daeUseSequencedTextureUnits");

    osgDB::ReaderWriter::ReadResult result = reader->readNode(filename, options);

    if (!result.success())
    {
        std::stringstream errormsg;
        errormsg << "Error loading file " << filename << ": " << result.message() << " code " << result.status() << std::endl;
        throw std::runtime_error(errormsg.str());
    }
    osg::ref_ptr<osg::Node> loadedModel = result.getNode();

    if(!loadedModel)
    {
        std::cout << "No data loaded!" << std::endl;
        return 1;
    }

    widget->getView(0)->setSceneData(loadedModel);

    main->show();

    return app.exec();
}
