// Copyright (C) 2020 by Nelsson Huotari

/* This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

#include <QApplication>
#include <QPointer>
#include <QWidget>
#include <QTimer>

#include <osgViewer/View>
#include "CompositeOsgRenderer.hpp"

class osgQOpenGLWidget;

class RenderWidget : public QWidget
{
        Q_OBJECT

    public:
        RenderWidget(QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
        virtual ~RenderWidget();
        osgViewer::View* getView(int viewNumber);
        QPointer<osgQOpenGLWidget> getOsgQOpenGLWidget();
        //void show();

    protected:

        QPointer<osgQOpenGLWidget> mWidget;
        CompositeOsgRenderer* mRenderer;
        osg::ref_ptr<osgViewer::View> mView;
        osg::ref_ptr<osg::Group> mRootNode;

        QTimer mTimer;
};
