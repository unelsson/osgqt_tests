#ifndef OSGQOPENGLWIDGET_H
#define OSGQOPENGLWIDGET_H

#ifdef __APPLE__
#   define __glext_h_
#   include <QtGui/qopengl.h>
#   undef __glext_h_
#   include <QtGui/qopenglext.h>
#endif

#include <mutex>

#ifdef WIN32
//#define __gl_h_
#include <osg/GL>
#endif

#include <osg/ArgumentParser>

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QReadWriteLock>

class CompositeOsgRenderer;

namespace osgViewer
{
    class View;
    class GraphicsWindowEmbedded;
}

class osgQOpenGLWidget : public QOpenGLWidget,
    protected QOpenGLFunctions
{
    Q_OBJECT

protected:
    CompositeOsgRenderer* m_renderer {nullptr};
    bool _osgWantsToRenderFrame{true};
    std::mutex _osgMutex;
    osg::ArgumentParser* _arguments {nullptr};
	bool _isFirstFrame {true};

    friend class CompositeOsgRenderer;

public:
    osgQOpenGLWidget(QWidget* parent = nullptr);
    osgQOpenGLWidget(osg::ArgumentParser* arguments, QWidget* parent = nullptr);
    virtual ~osgQOpenGLWidget();

    /** Get osgViewer View */
    virtual osgViewer::View* getOsgView(unsigned i);

    //! get mutex
    virtual std::mutex* mutex();

    CompositeOsgRenderer* getCompositeViewer();

    void setGraphicsWindowEmbedded(osg::ref_ptr<osgViewer::GraphicsWindowEmbedded> osgWinEmb);

signals:
    void initialized();

protected:

    //! call createRender. If overloaded, this method must send initialized signal at end
    void initializeGL() override;

    void resizeGL(int w, int h) override;

    //! lock scene graph and call osgViewer::frame()
    void paintGL() override;

    //! called before creating renderer
    virtual void setDefaultDisplaySettings();

    void keyPressEvent(QKeyEvent* event) override;
    void keyReleaseEvent(QKeyEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseDoubleClickEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void wheelEvent(QWheelEvent* event) override;

    void createRenderer();

private:
};

#endif // OSGQOPENGLWIDGET_H
