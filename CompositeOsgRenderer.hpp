// Copyright (C) 2020 by Nelsson Huotari (modification of OSGRenderer by Mike Krus)

/* This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

#ifndef COMPOSITEOSGRENDERER_H
#define COMPOSITEOSGRENDERER_H

#include <QObject>
#include <QTimer>
#include <QWheelEvent>

#include <osgViewer/CompositeViewer>

class CompositeOsgRenderer : public QObject, public osgViewer::CompositeViewer
{
    bool                                       mOsgInitialized {false};
    osg::ref_ptr<osgViewer::GraphicsWindowEmbedded>    mOsgWinEmb;
    osg::Timer                                 mFrameTimer;
    bool                                       mApplicationAboutToQuit {false};
    double                                     mSimulationTime;

    Q_OBJECT

public:

    explicit CompositeOsgRenderer(QObject* parent = nullptr);
    explicit CompositeOsgRenderer(osg::ArgumentParser* arguments, QObject* parent = nullptr);

    ~CompositeOsgRenderer() override;

    virtual void resize(int windowWidth, int windowHeight);

    virtual void keyPressEvent(QKeyEvent* event);
    virtual void keyReleaseEvent(QKeyEvent* event);
    virtual void mousePressEvent(QMouseEvent* event);
    virtual void mouseReleaseEvent(QMouseEvent* event);
    virtual void mouseDoubleClickEvent(QMouseEvent* event);
    virtual void mouseMoveEvent(QMouseEvent* event);
    virtual void wheelEvent(QWheelEvent* event);

    void setGraphicsWindowEmbedded(osg::ref_ptr<osgViewer::GraphicsWindowEmbedded> osgWinEmb);

    void setupOSG(int windowWidth, int windowHeight);

    // overrided from osgViewer::ViewerBase
    void frame(double simulationTime = USE_REFERENCE_TIME) override;

    QTimer mTimer;

protected:
    void timerEvent(QTimerEvent* event) override;
    void setKeyboardModifiers(QInputEvent* event);

public slots:
    void update();

signals:
    void simulationUpdated(double dt);
};

#endif // COMPOSITEOSGRENDERER_H
